package juegodelavida;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class JuegoDeLaVida extends Frame {

    int matriz[][] = new int[50][50];

    public JuegoDeLaVida() {
        setTitle("Juego De La Vida");
        setSize(600, 600);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        rellenarMatriz(matriz);
        add(new Escenario(matriz));
    }

    public void rellenarMatriz(int m[][]) {
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                m[i][j] = (int) (Math.random() * 2);
            }
        }
    }

    public static void main(String[] args) {
        new JuegoDeLaVida();
    }
}

class Escenario extends Canvas implements Runnable {

    int matriz[][] = new int[50][50];
    int maxX, maxY, minMaxXY, xCenter, yCenter;
    float ancho, alto, pixelSize;

    public Escenario(int matriz[][]) {
        this.matriz = matriz;
        new Thread(this).start();
    }

    void initgr() {
        Dimension d = getSize();
        maxX = d.width - 1;
        maxY = d.height - 1;
        xCenter = maxX / 2;
        yCenter = maxY / 2;
        ancho = 50;
        alto = 50;
        pixelSize = Math.max(ancho / maxX, alto / maxY);
    }

    int iX(float x) {
        return Math.round(xCenter + x / pixelSize);
    }

    int iY(float y) {
        return Math.round(yCenter + y / pixelSize);
    }

    public int[][] reglas(int m[][]) {
        int mA[][] = new int[m.length][m[0].length];
        for (int i = 1; i < m.length - 1; i++) {
            for (int j = 1; j < m[0].length - 1; j++) {

                int conV = m[i - 1][j - 1] + m[i - 1][j] + m[i - 1][j + 1]
                        + m[i][j - 1] + m[i][j + 1]
                        + m[i + 1][j - 1] + m[i + 1][j] + m[i + 1][j + 1];
                if (conV == 3) {
                    mA[i][j] = 1;
                } else if (conV != 2) {
                    mA[i][j] = 0;
                } else {
                    mA[i][j] = m[i][j];
                }

            }
        }
        return mA;
    }

    public void paint(Graphics g) {
        initgr();
        for (int x = -25; x <= 25; x++) {
            g.drawLine(iX(-25), iY(x), iX(25), iY(x));
            g.drawLine(iX(x), iY(-25), iX(x), iY(25));
        }
        int auxX = -25, auxY;
        for (int i = 0; i < matriz.length; i++) {
            auxY = -25;
            for (int j = 0; j < matriz[0].length; j++) {
                auxY++;
                if (matriz[i][j] == 1) {
                    g.fillRect(iX(auxX), iY(auxY), 13, 13);
                }
            }
            auxX++;
        }
        matriz = reglas(matriz);
    }
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
            repaint();
        }
    }
}
